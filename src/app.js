import express from "express";
import "express-async-errors";
import { initArtistsTable } from "./db/artistsDao.js";
import { initAlbumsTable } from "./db/albumsDao.js";
import { errorHandler } from "./utils/errorHandler.js";
import artistRouter from "./controllers/artists.js";
import albumsRouter from "./controllers/albums.js";

const { PORT, NODE_ENV } = process.env;

//init db tables
initArtistsTable();
initAlbumsTable();
//express app
const app = express();
app.use(express.json());
//routes
app.use("/artists", artistRouter);
app.use("/albums", albumsRouter);
//error middleware
app.use(errorHandler);

//serve
if (NODE_ENV !== "test") {
  app.listen(PORT, () => { 
    console.log(`Listening port ${PORT}`);
  });
}

export default app;