import { executeQuery } from "./db.js";

export const initAlbumsTable = async () => {
  if (process.env.NODE_ENV === "test") return null;
  const query = `
    CREATE TABLE IF NOT EXISTS "albums" (
      "id" SERIAL,
      "name" varchar(200) NOT NULL,
      "release_year" SMALLINT NOT NULL,
      "artist" INTEGER NOT NULL REFERENCES artists("id") ON DELETE CASCADE,
      PRIMARY KEY("id")
    );
  `;
  await executeQuery(query);
};

const listAll = async () => {
  const query = `
    SELECT
      albums.id,
      albums.name,
      albums.release_year,
      artists.name AS artist
    FROM albums
    JOIN artists ON artists.id = albums.artist;
  `;
  return await executeQuery(query);
};

const getOne = async valuesArr => {
  const query = `
    SELECT
      id, name, release_year, artist
    FROM albums
    WHERE id = $1;
  `;
  return await executeQuery(query, valuesArr);
}

const insertNew = async valuesArr => {
  const query = `
    INSERT INTO albums (name, release_year, artist)
    VALUES ($1, $2, $3)
    RETURNING id, name, release_year, artist;
  `;
  return await executeQuery(query, valuesArr);
};

const updateOne = async valuesArr => {
  const query = `
    UPDATE albums
    SET name = $2, release_year = $3, artist = $4
    WHERE id = $1
    RETURNING id, name, release_year, artist;
  `;
  return await executeQuery(query, valuesArr);
};

const deleteOne = async valuesArr => {
  const query = `
    DELETE FROM albums
    WHERE id = $1
  `;
  return await executeQuery(query, valuesArr);
}

export default {
  listAll,
  getOne,
  insertNew,
  updateOne,
  deleteOne
}

