import { executeQuery } from "./db.js";

export const initArtistsTable = async () => {
  if (process.env.NODE_ENV === "test") return null;
  const query = `
    CREATE TABLE IF NOT EXISTS "artists" (
      "id" SERIAL,
      "name" varchar(100) NOT NULL,
      PRIMARY KEY("id")
    );
  `;
  await executeQuery(query);
};

const listAll = async () => {
  const query = "SELECT id, name FROM artists;";
  return await executeQuery(query);
};

const getOne = async () => {
  const query = "SELECT id, name FROM artists WHERE id = $1;";
  return await executeQuery(query);
};

const insertNew = async valuesArr => {
  const query = `
    INSERT INTO artists (name) VALUES ($1)
    RETURNING id, name;
  `;
  return await executeQuery(query, valuesArr);
};

const updateOne = async valuesArr => {
  const query = `
    UPDATE artists
    SET name = $2
    WHERE id = $1
    RETURNING id, name;
  `;
  return await executeQuery(query, valuesArr);
};

const deleteOne = async valuesArr => {
  const query = `
    DELETE FROM artists
    WHERE id = $1
  `;
  return await executeQuery(query, valuesArr);
}

export default {
  initArtistsTable,
  listAll,
  getOne,
  insertNew,
  updateOne,
  deleteOne
}