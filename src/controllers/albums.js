import { Router } from "express";
import albumsDao from "../db/albumsDao.js";

const router = Router();

router.get("/", async (_req, res) => {
  const dbRes = await albumsDao.listAll();
  res.send(dbRes.rows);
})

router.post("/", async (req, res) => {
  const { name, releaseYear, artist } = req.body;
  if (!name || !releaseYear || !artist ) {
    throw new Error('Missing one or many of required params: name, releaseYear, artist');
  }
  const dbRes = await albumsDao.insertNew([name, releaseYear, artist]);
  res.send(dbRes.rows[0]);
});

router.put("/:id", async (req, res) => {
  const { name, releaseYear, artist } = req.body;
  //next line throws 'ReferenceError' if no records are found
  const albumsInDb = await albumsDao.getOne([req.params.id]);
  const album = albumsInDb.rows[0];
  const dbRes = await albumsDao.updateOne([
    album.id,
    name || album.name,
    releaseYear || album.release_year,
    artist || album.artist
  ]);
  res.send(dbRes.rows[0]);
});

router.delete("/:id", async (req, res) => {
  await albumsDao.deleteOne([req.params.id]);
  res.status(204).end();
});

export default router;