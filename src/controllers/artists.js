import { Router } from "express";
import artistsDao from "../db/artistsDao.js";

const router = Router();

router.get("/", async (_req, res) => {
  const dbRes = await artistsDao.listAll();
  res.send(dbRes.rows);
});

router.post("/", async (req, res) => {
  const { name } = req.body;
  if (!name) throw new Error('Missing required param name');
  const dbRes = await artistsDao.insertNew([name]);
  res.send(dbRes.rows[0]);
});

router.put("/:id", async (req, res) => {
  const { name } = req.body;
  if (!name) throw new Error('Missing required param name');

  const dbRes = await artistsDao.updateOne([
    req.params.id, name
  ]);
  //Would be good to check the existance beforehand tho
  if (!dbRes.rowCount) {
    throw new Error(`Artists with id ${req.params.id} not found`);
  }
  res.send(dbRes.rows[0]);
});

router.delete("/:id", async (req, res) => {
  await artistsDao.deleteOne([req.params.id]);
  res.status(204).end();
});

export default router;