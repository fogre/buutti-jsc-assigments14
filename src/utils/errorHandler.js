//just a quick 'fake' error handler to remind me how to do this
export const errorHandler = (error, _req, res, next) => {
  console.log(error.message);
  switch(error.name) {
  case "Error":
    return res.status(400).send({ error: error.message });
  case "ReferenceError":
    return res.status(400).send({ error: `Invalid req.params.id: ${error.message}` });
  default: 
    next(error);
  }
};