import request from "supertest";
import app from "../app.js";
import { pool } from "../db/db.js";
import { mockPool } from "./testHelpers.js";

describe("When using GET /albums", () => {
  const mockRes = {
    rows: [
      { id: 1, name: "ding", release_year: 2, artist: "name" },
      { id: 2, name: "donger album", release_year: 10, artist: "aaa" }
    ]
  };

  beforeEach(() => {
    pool.connect = mockPool(mockRes);
  });

  it("Returns 200 for all albums when on route /", async () => {
    const res = await request(app).get("/albums");
    expect(res.status).toBe(200);
    expect(res.body).toEqual(mockRes.rows);
  });
});