import request from "supertest";
import app from "../app.js";
import { pool } from "../db/db.js";
import { mockPool } from "./testHelpers.js";

describe("When using GET /artists", () => {
  const mockRes = {
    rows: [
      { id: 1, name: "artist" },
      { id: 2, name: "another artists" }
    ]
  };

  beforeEach(() => {
    pool.connect = mockPool(mockRes);
  });

  it("Returns 200 for all artists when on route /", async () => {
    const res = await request(app).get("/artists");
    expect(res.status).toBe(200);
    expect(res.body).toEqual(mockRes.rows);
  });
});