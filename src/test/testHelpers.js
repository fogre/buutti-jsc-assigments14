import { jest } from "@jest/globals";

export const mockPool = mockRes => jest.fn(() => {
  return {
    query: () => mockRes,
    release: () => null
  };
});