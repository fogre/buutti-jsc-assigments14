FROM node:16-alpine

ARG APP_PORT
ARG PG_DATABASE
ARG PG_HOST
ARG PG_PASSWORD
ARG PG_PORT
ARG PG_USERNAME

ENV PG_DATABASE=${PG_DATABASE}
ENV PG_HOST=${PG_HOST}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PORT=${APP_PORT}

WORKDIR /usr/src/app
COPY src/package*.json ./
RUN npm ci --omit=dev

COPY src/app.js .
COPY src/controllers/ ./controllers/
COPY src/db/ ./db/
COPY src/utils/ ./utils/

EXPOSE ${APP_PORT}

CMD ["npm", "start"]