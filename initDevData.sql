CREATE TABLE IF NOT EXISTS artists (
  id SERIAL,
  name varchar(100) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS albums (
  id SERIAL,
  name varchar(200) NOT NULL,
  release_year SMALLINT NOT NULL,
  artist INTEGER NOT NULL REFERENCES artists(id) ON DELETE CASCADE,
  PRIMARY KEY(id)
);

INSERT INTO
  artists (name)
VALUES
  ('Juice Leskinen'),
  ('The Rolling Stones'),
  ('Antti Tuisku'),
  ('Elvis Presley'),
  ('Evelina');

INSERT INTO
  albums (name, artist, release_year)
VALUES
  ('Taivaan Kappaleita', 1, 1991),
  ('Black and Blue', 2, 1976),
  ('En Kommentoi', 3, 2015),
  ('Elvis is Back!', 4, 1960),
  ('III', 5, 2020);